gitso (0.6.2+svn158+dfsg-5) UNRELEASED; urgency=medium

  * Drop fields with obsolete URLs.
  * Update standards version to 4.6.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 02 Feb 2022 06:06:41 -0000

gitso (0.6.2+svn158+dfsg-4) unstable; urgency=medium

  * Add missing dependency on net-tools (thanks, Didier Lebrun!)
  * Bump dh compat to level 13
  * Add Rules-Requires-Root: no
  * Declare compliance with Debian Policy 4.5.0

 -- Florian Schlichting <fsfs@debian.org>  Fri, 25 Sep 2020 01:38:13 +0800

gitso (0.6.2+svn158+dfsg-3) unstable; urgency=medium

  [ Scott Talbert ]
  * Port to Python 3 / wxPython 4 (closes: #943042)

  [ Florian Schlichting ]
  * Fix a remaining python -> python3 shebang
  * Silence lintian warnings about the Google code archive as long as there is
    no better upstream
  * Update years of packaging copyright
  * Bump dh compat to level 12
  * Declare compliance with Debian Policy 4.4.1

 -- Florian Schlichting <fsfs@debian.org>  Thu, 05 Dec 2019 15:35:42 +0800

gitso (0.6.2+svn158+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces

  [ Florian Schlichting ]
  * gitso.desktop: no MimeType as we do not support any file input
  * Convert desktop icon to png, this is required by the freedesktop.org spec
  * Increase connection window dimensions (closes: #760071)
  * Google Code is history, update links to point to the archive and remove
    watch file and get-orig-source target
  * Update packaging copyright years
  * Bump dh compat to level 11, use debhelper-compat dependency
  * Declare compliance with Debian Policy 4.3.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 31 Dec 2018 12:17:57 +0100

gitso (0.6.2+svn158+dfsg-1) unstable; urgency=medium

  * First upload to Debian (closes: #605985, LP: 319444)
  * Takeover:
    + Set myself as maintainer
    + Update Vcs-* fields to point to Alioth
    + Use copyright stanza from AboutWindow.py, and credit upstream deb
      packagers
  * Update packaging:
    + Switch to source format 3.0 (quilt)
    + Switch to dh compat level 9
    + Add dependencies on ${misc:Depends} and python
    + Rewrite short and long description
    + Add get-orig-source target to debian/rules
    + Add a watch file (for the svn revision number)
    + Switch to copyright-format 1.0, adding a comment about +dfsg version
    + Add copyright paragraph for NATPMP.py
    + Declare compliance with Debian Policy 3.9.5
  * Patches:
    + New fix_desktop_file.patch, adding a Keywords entry
    + New hyphen-used-as-minus-sign.patch
    + Use /etc/gitso-hosts as system-wide configuration file
    + Create ~/.gitso-hosts if it doesn't exist
    + Display GPL-3 from common-licenses instead of our own
    + Add HOWTO from the code.google.com wiki

 -- Florian Schlichting <fsfs@debian.org>  Thu, 16 Jan 2014 22:48:36 +0100

gitso (0.6.2) precise; urgency=low

  * Cleaned up .deb creation.
  * Remove CHANGELOG.txt.
  * Make gitso executable.
  * Added homepage to .deb control file.
  * Changed vncviewer dependency to xtightvncviewer.
  * Fix package name in linux changelog.
  * Added 8 bit color compression.

 -- Markus Roth <markus.roth@herr-biber.de>  Thu, 27 Sep 2012 03:06:54 +0200

gitso (0.6) karmic; urgency=low

  * Complete rewrite of process management.
  * Actually stop VNC Processes (Windows)
  * Support loading remote hosts file.
  * Command line switches
  *     --dev
  *     --listen
  *     --connect IP
  *     --list list_file
  *     --version
  *     --help
  * manpage for (All UNIX sytems)
  * Support for .rpms (Fedora, OpenSUSE)
  * Implement Native VNC listener (OS X)
  * Better process management, user gets notified if connection is broken.
  * Licensing Updates (across the board).
  * Improved documentation.

 -- Aaron D. Gerber <gerberad@gmail.com>  Sun, 21 Feb 2010 17:32:40 -0600

gitso (0.5) hardy; urgency=low

  * Complete rewrite of the interface
  * Gitso no longer has Zombied VNC processes after it quits.
  * Gitso stops the VNC process when it closes
  * Updated Icon
  * Updated License: GPL 3
  * Added Support to be able to specify a list of hosts when you distribute it.
  * Added History/Clear History of servers
  * Added OS X 10.5 Support (need testing on 10.4 and 10.3)
  *   OS X uses TightVNC 1.3.9
  *   OS X uses OSXVNC 3.0

 -- Aaron D. Gerber <gerberad@gmail.com>  Sat, 26 Jul 2008 16:32:40 -0600

gitso (0.4) hardy; urgency=low

  * Made Deb, updated with icons and much more.

 -- Aaron D. Gerber <gerberad@gmail.com>  Sat, 10 May 2008 16:17:43 -0600

gitso (0.3) UNRELEASED; urgency=low

  * Initial release.

 -- Aaron D. Gerber <gerberad@gmail.com>  Thu, 08 May 2008 22:35:52 -0600
